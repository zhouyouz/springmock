package com.jason.servlet;

import com.jason.annotation.Autowird;
import com.jason.annotation.Controller;
import com.jason.annotation.RequestMapping;
import com.jason.annotation.Service;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhouyouz@163.com
 * @Title: ${file_name}
 * @Description: spring模拟
 * @date 2018/8/18 11:12
 */
public class SpringMockServlet extends HttpServlet {

    private List<String> clazzNames = new ArrayList<>();

    private Map<String,Object> beans = new HashMap<>();

    private Map<String,Object> handlerMapping = new HashMap<>();

    @Override
    public void init(ServletConfig config) throws ServletException {

        try {
            //包扫描
            this.scanPackage("com.jason");

            //实例化
            this.createInstance();

            //依赖注入
            this.injection();

            //请求路径和类-方法映射
            this.handlerMapping();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 路径映射
     */
    private void handlerMapping() {
        if (beans.isEmpty()){
            return;
        }

        for (Map.Entry<String,Object> entry : beans.entrySet()){
            Object instance = entry.getValue();
            Class<?> clszz = instance.getClass();
            if (clszz.isAnnotationPresent(Controller.class)){

                //类上请求路径
                String classMapping = "";
                if (clszz.isAnnotationPresent(RequestMapping.class)){
                    RequestMapping rp = clszz.getAnnotation(RequestMapping.class);
                    classMapping = rp.value();
                }

                Method[] methods = clszz.getMethods();
                for (Method method : methods){
                    if (method.isAnnotationPresent(RequestMapping.class)){
                        RequestMapping rp = method.getAnnotation(RequestMapping.class);
                        String methodMapping = rp.value();

                        //路径 /test/hello => test()
                        handlerMapping.put(classMapping+methodMapping,method);
                    }
                }
            }
        }
    }

    /**
     * 为实例属性注入对象
     */
    private void injection() throws Exception {
        if (beans.isEmpty()){
            return;
        }

        //遍历beans所有实例，对使用了Autowired注解的属性进行注入
        for (Map.Entry<String,Object> entry : beans.entrySet()){
            //取出实例
            Object instance = entry.getValue();

            //取出所有字段
            Field[] fields = instance.getClass().getDeclaredFields();
            for (Field field : fields){

                //找到所有使用@Autowired的属性
                if (field.isAnnotationPresent(Autowird.class)){
                    Autowird autowird = field.getAnnotation(Autowird.class);
                    String value = autowird.value();

                    //私有属性需要打破封装才能设置值
                    field.setAccessible(true);
                    field.set(instance,beans.get(value));
                }
            }
        }
    }

    /**
     * 创建实例
     * @throws Exception
     */
    private void createInstance()throws Exception{
        if (clazzNames.isEmpty()){
            return;
        }

        for (String clazzpath : clazzNames){

            //加载类只需要使用类的全路径，不需要后缀.class
            clazzpath = clazzpath.replace(".class","");

            //完成类加载
            Class<?> clazz = Class.forName(clazzpath);

            //实例化对象@Controller 和@Service
            if (clazz.isAnnotationPresent(Controller.class)){
                Object instance = clazz.newInstance();

                //类上注解  /test => SpringMockController
                RequestMapping requestMapping = clazz.getAnnotation(RequestMapping.class);
                String value = requestMapping.value();

                //保存路径和实例映射关系
                beans.put(value,instance);
            }

            if (clazz.isAnnotationPresent(Service.class)){
                Object instance = clazz.newInstance();
                Service service = clazz.getAnnotation(Service.class);
                String value = service.value();

                //实例名称和实例映射关系
                beans.put(value,instance);
            }
        }
    }

    /**
     * 包扫描
     * @param packageName
     * @throws Exception
     */
    private void scanPackage(String packageName) throws Exception{
        if (packageName==null || packageName.equals("")){
            throw new Exception("packageName cannot be null!");
        }

        String path = packageName.replaceAll("\\.", File.separator);
        URL url = this.getClass().getClassLoader().getResource(path);
        File[] files = new File(url.getPath()).listFiles();
        for (File file : files){
            if (file.isDirectory()){
                scanPackage(packageName+"."+file.getName());
            }else {
                String clazzPath = file.getPath();
                if (clazzPath.endsWith(".class")){
                    clazzPath = clazzPath.replace(url.getPath(),packageName+".");
                    clazzNames.add(clazzPath);
                }
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        uri = uri.replace("/springmock","");

        String context = req.getContextPath();
        String path = uri.replaceAll(context,"");

        System.out.println(uri+","+context+","+path);

        //根据路径找到对应方法
        Method method = (Method) handlerMapping.get(path);

        Object instance = beans.get("/"+path.split("/")[1]);

        try{
            if (method != null && instance != null) {
                method.invoke(instance, null);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       this.doPost(req,resp);
    }
}
