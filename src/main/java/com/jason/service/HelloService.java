package com.jason.service;

import com.jason.annotation.Service;

/**
 * @author zhouyouz@163.com
 * @Title: ${file_name}
 * @Description: ${todo}
 * @date 2018/8/18 11:21
 */
@Service(value = "helloService")
public class HelloService {
    public String getHello(){
        return "hello!";
    }
}
