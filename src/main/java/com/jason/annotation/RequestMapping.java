package com.jason.annotation;

import java.lang.annotation.*;

/**
 * @author zhouyouz@163.com
 * @Title: ${file_name}
 * @Description: ${todo}
 * @date 2018/8/18 11:25
 */

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestMapping {
    String value() default "";
}
