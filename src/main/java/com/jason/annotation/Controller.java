package com.jason.annotation;

import java.lang.annotation.*;

/**
 * @author zhouyouz@163.com
 * @Title: ${file_name}
 * @Description: ${todo}
 * @date 2018/8/18 11:22
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Controller {
    String value() default "";
}
