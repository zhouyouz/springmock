package com.jason.request;

import com.jason.annotation.Autowird;
import com.jason.annotation.Controller;
import com.jason.annotation.RequestMapping;
import com.jason.service.HelloService;


/**
 * @author zhouyouz@163.com
 * @Title: ${file_name}
 * @Description: ${todo}
 * @date 2018/8/18 11:19
 */

@Controller
@RequestMapping(value = "/test")
public class SpringMockController {

    @Autowird(value = "helloService")
    private HelloService helloService;

    @RequestMapping(value = "/hello")
    public String test(){
        System.out.println("springmock method invoke succeed!");
        return helloService.getHello();
    }
}
